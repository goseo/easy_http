package easy_http

import (
	"bytes"
	"encoding/json"
	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/transform"
	"io/ioutil"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strings"
)

func (s Config) PostJson(_url string, post_data interface{}) (e2 error, data string) {
	var e error
	client := http.Client{
		Timeout: s.TimeOut,
	}
	if s.ProxyUrl != "" {
		client.Transport = &http.Transport{
			Proxy: func(_ *http.Request) (url *url.URL, e error) {
				return url.Parse(s.ProxyUrl)
			},
		}
	}
	marshal, e2 := json.Marshal(post_data)
	if e2 != nil {
		return
	}
	//fmt.Printf("%s\n",marshal)
	request, e := http.NewRequest("POST", _url, bytes.NewReader(marshal))
	if e != nil {
		e2 = e
		return
	}
	//自动保存Cookie
	if s.SaveCookie {
		jar, e := cookiejar.New(nil)
		if e != nil {
			e2 = e
			return
		}
		client.Jar = jar
	}
	//设置UA头
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Ua", s.Ua)
	request.Header.Set("Cookie", s.Cookie)
	//添加自定义的Headers
	if len(s.Headers) != 0 {
		for h_k, h_v := range s.Headers {
			request.Header.Set(h_k, h_v)
		}
	}
	//发送请求
	response, e := client.Do(request)
	if e != nil {
		e2 = e
		return
	}
	defer response.Body.Close()
	b, e := ioutil.ReadAll(response.Body)
	if e != nil {
		e2 = e
		return
	}
	//判断头部是否已给出编码
	content_type := response.Header.Get("content-type")
	lower := strings.ToLower(content_type)
	switch {
	case strings.Count(lower, "utf-8") > 0:
		data = string(b)
		return
	case strings.Count(lower, "gb") > 0:
		_decode := simplifiedchinese.GBK.NewDecoder()
		r := transform.NewReader(bytes.NewReader(b), _decode)
		bck, _ := ioutil.ReadAll(r)
		e = nil
		data = string(bck)
		return
	}
	//判断头部是否已给出编码 END

	//编码转换
	encoding, name, _, e := DetermineEncodingFromReader(bytes.NewReader(b), len(b))
	if e != nil {
		e2 = e
		return
	}
	_decode := encoding.NewDecoder()
	switch name {
	case "windows-1252":
		_decode = simplifiedchinese.GBK.NewDecoder()
	}
	r := transform.NewReader(bytes.NewReader(b), _decode)
	bck, _ := ioutil.ReadAll(r)
	e = nil
	data = string(bck)
	return
}

func (s Config) Post(_url string, post_data url.Values) (e2 error, data string) {
	var e error
	client := http.Client{
		Timeout: s.TimeOut,
	}
	if s.ProxyUrl != "" {
		client.Transport = &http.Transport{
			Proxy: func(_ *http.Request) (url *url.URL, e error) {
				return url.Parse(s.ProxyUrl)
			},
		}
	}
	post := post_data.Encode()
	//fmt.Printf("%s\n",marshal)
	request, e := http.NewRequest("POST", _url, strings.NewReader(post))
	if e != nil {
		e2 = e
		return
	}
	//自动保存Cookie
	if s.SaveCookie {
		jar, e := cookiejar.New(nil)
		if e != nil {
			e2 = e
			return
		}
		client.Jar = jar
	}
	//设置UA头
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	request.Header.Set("Ua", s.Ua)
	request.Header.Set("Cookie", s.Cookie)
	//添加自定义的Headers
	if len(s.Headers) != 0 {
		for h_k, h_v := range s.Headers {
			request.Header.Set(h_k, h_v)
		}
	}
	//发送请求
	response, e := client.Do(request)
	if e != nil {
		e2 = e
		return
	}
	defer response.Body.Close()
	b, e := ioutil.ReadAll(response.Body)
	if e != nil {
		e2 = e
		return
	}
	//判断头部是否已给出编码
	content_type := response.Header.Get("content-type")
	lower := strings.ToLower(content_type)
	switch {
	case strings.Count(lower, "utf-8") > 0:
		data = string(b)
		return
	case strings.Count(lower, "gb") > 0:
		_decode := simplifiedchinese.GBK.NewDecoder()
		r := transform.NewReader(bytes.NewReader(b), _decode)
		bck, _ := ioutil.ReadAll(r)
		e = nil
		data = string(bck)
		return
	}
	//判断头部是否已给出编码 END

	//编码转换
	encoding, name, _, e := DetermineEncodingFromReader(bytes.NewReader(b), len(b))
	if e != nil {
		e2 = e
		return
	}
	_decode := encoding.NewDecoder()
	switch name {
	case "windows-1252":
		_decode = simplifiedchinese.GBK.NewDecoder()
	}
	r := transform.NewReader(bytes.NewReader(b), _decode)
	bck, _ := ioutil.ReadAll(r)
	e = nil
	data = string(bck)
	return
}
