package easy_http

import (
	"bufio"
	"golang.org/x/net/html/charset"
	"golang.org/x/text/encoding"
	"io"
)

func DetermineEncodingFromReader(r io.Reader, peek int) (e encoding.Encoding, name string, certain bool, err error) {
	if peek >= 1024 {
		peek = 1024
	}
	b, err := bufio.NewReader(r).Peek(peek)
	if err != nil {
		return
	}
	e, name, certain = charset.DetermineEncoding(b, "")
	return
}
