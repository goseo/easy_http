package easy_http

import (
	"bytes"
	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/encoding/traditionalchinese"
	"golang.org/x/text/transform"
	"io/ioutil"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strings"
	"time"
)

type Config struct {
	SaveCookie  bool
	Ua          string
	TimeOut     time.Duration
	FromCharset string
	OutCharset  string
	Cookie      string
	Headers     map[string]string
	ProxyUrl    string
}

func (s Config) init() {
	s.SaveCookie = true

}

//默认配置项 需要得到config 通过这个去发请求
func GetConfig() Config {
	return Config{
		SaveCookie: true,
		Ua:         "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36",
		TimeOut:    time.Second * 60,
	}
}

func (s Config) Get(_url string) (e2 error, data string) {
	var e error
	var request *http.Request
	var response *http.Response
	client := http.Client{
		Timeout: s.TimeOut,
	}
	if s.ProxyUrl != "" {
		client.Transport = &http.Transport{
			Proxy: func(_ *http.Request) (url *url.URL, e error) {
				return url.Parse(s.ProxyUrl)
			},
		}
	}
	request, e = http.NewRequest("GET", _url, nil)
	if e != nil {
		return
	}
	//自动保存Cookie
	if s.SaveCookie {
		jar, e := cookiejar.New(nil)
		if e != nil {
			return
		}
		client.Jar = jar
	}
	//设置UA头
	request.Header.Set("Ua", s.Ua)
	request.Header.Set("Cookie", s.Cookie)
	//添加自定义的Headers
	if len(s.Headers) != 0 {
		for h_k, h_v := range s.Headers {
			request.Header.Set(h_k, h_v)
		}
	}
	//发送请求
	response, e = client.Do(request)
	if e != nil {
		return
	}
	defer response.Body.Close()
	b, e := ioutil.ReadAll(response.Body)
	if e != nil {
		e2 = e
		return
	}

	//判断头部是否已给出编码
	content_type := response.Header.Get("content-type")
	lower := strings.ToLower(content_type)
	switch {
	case strings.Count(lower, "utf-8") > 0:
		data = string(b)
		return
	case strings.Count(lower, "big5") > 0:
		r := transform.NewReader(bytes.NewReader(b), traditionalchinese.Big5.NewDecoder())
		bck, _ := ioutil.ReadAll(r)
		e = nil
		data = string(bck)
		return
	case strings.Count(lower, "gb") > 0:
		_decode := simplifiedchinese.GBK.NewDecoder()
		r := transform.NewReader(bytes.NewReader(b), _decode)
		bck, _ := ioutil.ReadAll(r)
		e = nil
		data = string(bck)
		return
	}
	//判断头部是否已给出编码 END

	//编码转换
	encoding, name, _, e := DetermineEncodingFromReader(bytes.NewReader(b), len(b))
	if e != nil {
		e2 = e
		return
	}
	_decode := encoding.NewDecoder()
	switch name {
	case "windows-1252":
		_decode = simplifiedchinese.GBK.NewDecoder()
	}
	r := transform.NewReader(bytes.NewReader(b), _decode)
	bck, _ := ioutil.ReadAll(r)
	e = nil
	data = string(bck)
	return
}

func (s Config) GetRef(_url string) (e2 error, data string, ref string) {
	var e error
	client := http.Client{
		Timeout: s.TimeOut,
	}
	if s.ProxyUrl != "" {
		client.Transport = &http.Transport{
			Proxy: func(_ *http.Request) (url *url.URL, e error) {
				return url.Parse(s.ProxyUrl)
			},
		}
	}
	request, e := http.NewRequest("GET", _url, nil)
	if e != nil {
		e2 = e
		return
	}
	//自动保存Cookie
	if s.SaveCookie {
		jar, e := cookiejar.New(nil)
		if e != nil {
			e2 = e
			return
		}
		client.Jar = jar
	}
	//设置UA头
	request.Header.Set("Ua", s.Ua)
	request.Header.Set("Cookie", s.Cookie)
	//添加自定义的Headers
	if len(s.Headers) != 0 {
		for h_k, h_v := range s.Headers {
			request.Header.Set(h_k, h_v)
		}
	}
	//发送请求
	response, e := client.Do(request)

	if e != nil {
		e2 = e
		return
	}
	defer response.Body.Close()
	//得到Ref
	locaUrl := response.Request.URL.Scheme + "://" + response.Request.URL.Host + response.Request.URL.Path
	if response.Request.URL.RawQuery != "" {
		locaUrl += "?" + response.Request.URL.RawQuery
	}
	ref = locaUrl

	b, e := ioutil.ReadAll(response.Body)
	if e != nil {
		e2 = e
		return
	}
	//判断头部是否已给出编码
	content_type := response.Header.Get("content-type")
	lower := strings.ToLower(content_type)
	switch {
	case strings.Count(lower, "utf-8") > 0:
		data = string(b)
		return
	case strings.Count(lower, "big5") > 0:
		r := transform.NewReader(bytes.NewReader(b), traditionalchinese.Big5.NewDecoder())
		bck, _ := ioutil.ReadAll(r)
		e = nil
		data = string(bck)
		return
	case strings.Count(lower, "gb") > 0:
		_decode := simplifiedchinese.GBK.NewDecoder()
		r := transform.NewReader(bytes.NewReader(b), _decode)
		bck, _ := ioutil.ReadAll(r)
		e = nil
		data = string(bck)
		return
	}
	//判断头部是否已给出编码 END

	//编码转换
	encoding, name, _, e := DetermineEncodingFromReader(bytes.NewReader(b), len(b))
	if e != nil {
		e2 = e
		return
	}
	_decode := encoding.NewDecoder()
	switch name {
	case "windows-1252":
		_decode = simplifiedchinese.GBK.NewDecoder()
	}
	r := transform.NewReader(bytes.NewReader(b), _decode)
	bck, _ := ioutil.ReadAll(r)
	e = nil
	data = string(bck)
	return
}
